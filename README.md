# recipe-app-api-proxy

Recipe-app-api-proxy including nginx and CI/CD implementation

Nginx Proxy app Information

### Enviornment Variable 

* `LISTEN_PORT` - Port to listen to ( default = : `8000`)
* `APP_HOST` = Hostname for the app to forward to (default = `app`)
* `APP_PORT` = Port of the app to forward to (default `9000`)